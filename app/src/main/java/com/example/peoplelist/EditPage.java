package com.example.peoplelist;

import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import com.example.milestone4.MyFriends;
import com.example.milestone4.model.Contact;

public class EditPage extends AppCompatActivity {

    Button btn_exit, btn_add;

    ListView lv_friendsList;

    ContactAdapter adapter;
    MyFriends myFriends;

    BusinessService businessIndex;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_page);

        businessIndex = new BusinessService( getApplicationContext() );

        btn_exit = (Button) findViewById(R.id.btn_sortAGE);
        btn_add = (Button) findViewById(R.id.btn_add);
        lv_friendsList = findViewById(R.id.lv_friendsList);

        myFriends = ((MyApplication) this.getApplication()).getMyFriends();

        myFriends = businessIndex.ReadAllData("theContacts.json");

        adapter = new ContactAdapter(EditPage.this, myFriends);

        lv_friendsList.setAdapter(adapter);

        btn_exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                businessIndex.WriteAllData(myFriends, "theContacts.json" );
                myFriends = businessIndex.ReadAllData("theContacts.json");
                setResult(3);
                finish();
            }
        });

        btn_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(v.getContext(), NewContactForm.class );
                startActivityForResult(i, 2);
            }
        });

        lv_friendsList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                editContact(position);
            }
        });

    }

    private void editContact(int position) {
        Intent i = new Intent(getApplicationContext(), NewContactForm.class);

        //get the contents of contact at position
        Contact p = myFriends.getMyFriendsList().get(position);

        i.putExtra("edit", position);
        i.putExtra("name", p.getFirstName());
        i.putExtra("lastName", p.getLastName());
        i.putExtra("age", p.getAge());
        i.putExtra("phone", p.getPhoneNum());
        i.putExtra("pictureNumber", p.getPictureNumber());
        i.putExtra("address", p.getLoc());

        startActivityForResult(i, 2);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == 2){


            if (data != null) {

                //capture incoming data
                String name = data.getStringExtra("name");
                String lastName = data.getStringExtra("lastName");
                int age = Integer.parseInt(data.getStringExtra("age"));
                int pictureNumber = Integer.parseInt(data.getStringExtra("pictureNumber"));
                String phoneNum = data.getStringExtra("phone");
                String address = data.getStringExtra("address");
                int positionEdited = data.getIntExtra("edit", -1);
                boolean wasDeleted = data.getBooleanExtra("delete", false);

                if (wasDeleted == false) {
                    //create new contact object
                    Contact p = new Contact(name, lastName, age, pictureNumber, phoneNum, address);

                    //add contact to the list and update adapter
                    if (positionEdited > -1) {
                        myFriends.getMyFriendsList().remove(positionEdited);
                    }
                    myFriends.getMyFriendsList().add(p);

                }
                else {
                    if (positionEdited > -1){
                        myFriends.getMyFriendsList().remove(positionEdited);
                    }
                }
                adapter.notifyDataSetChanged();
            }
        }

    }
}
