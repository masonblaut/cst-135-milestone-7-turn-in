package com.example.peoplelist;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

public class ContactProfile extends AppCompatActivity {

    Button btn_callNum, btn_loc, btn_exit, btn_text;
    TextView tv_name, tv_age;
    ImageView iv_personicon;
    EditText et_textForm;

    int position = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_person_profile);

        btn_callNum = (Button) findViewById(R.id.btn_callNum);
        btn_loc = (Button) findViewById(R.id.btn_loc);
        btn_exit = (Button) findViewById(R.id.btn_exit);
        btn_text = (Button) findViewById(R.id.btn_txt);
        et_textForm = findViewById(R.id.et_textForm);

        tv_age = (TextView) findViewById(R.id.tv_age);
        tv_name = (TextView) findViewById(R.id.tv_name);

        iv_personicon = (ImageView) findViewById(R.id.iv_personicon);

        Bundle incomingMessages = getIntent().getExtras();

        if(incomingMessages != null){

            //capture incoming data
            String name = incomingMessages.getString("name");
            String lastName = incomingMessages.getString("lastName");
            String phone = incomingMessages.getString("phone");
            int age = incomingMessages.getInt("age");
            int picNum = incomingMessages.getInt("pictureNumber");
            String address = incomingMessages.getString("address");


            tv_name.setText(name+" "+lastName);
            tv_age.setText(Integer.toString(age));
            btn_callNum.setText(phone);
            btn_loc.setText(address);

            int icon_resource_numbers [] = {
                    R.drawable.bluedress,
                    R.drawable.bowtie,
                    R.drawable.brownhair,
                    R.drawable.businesssuit,
                    R.drawable.constructionworker,
                    R.drawable.fancydress,
                    R.drawable.fancysuit,
                    R.drawable.farmer,
                    R.drawable.girlglasses,
                    R.drawable.greenstripes,
                    R.drawable.greyhat,
                    R.drawable.greyshirt,
                    R.drawable.guyglasses,
                    R.drawable.reddress,
                    R.drawable.redhead,
                    R.drawable.suitandtie
            };
            iv_personicon.setImageResource(icon_resource_numbers[picNum]);
        }

        btn_callNum.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialPhoneNumber(btn_callNum.getText().toString());
            }
        });

        btn_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                composeMmsMessage(btn_callNum.getText().toString(), et_textForm.getText().toString());
            }
        });

        btn_loc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String mapsQuery ="geo:0,0?q="+btn_loc.getText().toString();
                Uri mapUri = Uri.parse(mapsQuery);
                showMap(mapUri);
            }
        });

        btn_exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }

    public void dialPhoneNumber(String phoneNumber) {
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:" + phoneNumber));
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        }
    }

    public void composeMmsMessage(String phoneNumber, String message) {
        Intent intent = new Intent(Intent.ACTION_SENDTO);
        intent.setData(Uri.parse("smsto:"+phoneNumber));  // This ensures only SMS apps respond
        intent.putExtra("sms_body", message);
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        }
    }

    public void showMap(Uri geoLocation) {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(geoLocation);
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        }
    }
}
