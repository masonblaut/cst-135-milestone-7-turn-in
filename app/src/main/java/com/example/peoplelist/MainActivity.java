package com.example.peoplelist;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.example.milestone4.MyFriends;
import com.example.milestone4.model.Contact;

import java.util.BitSet;
import java.util.Collections;
import java.util.Comparator;

public class MainActivity extends AppCompatActivity {

    Button btn_sortABC, btn_sortAGE, btn_edit, btn_search;
    ListView lv_friendsList;
    EditText et_search;

    ContactAdapter adapter;
    MyFriends myFriends;


    BusinessService businessIndex;
    //Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //context = getApplicationContext();

        businessIndex = new BusinessService( getApplicationContext() );

        btn_sortABC = (Button) findViewById(R.id.btn_sortABC);
        btn_sortAGE = (Button) findViewById(R.id.btn_sortAGE);
        btn_edit = (Button) findViewById(R.id.btn_edit);
        lv_friendsList = findViewById(R.id.lv_friendsList);
        btn_search = (Button) findViewById(R.id.btn_search);
        et_search = (EditText) findViewById(R.id.et_search);


        myFriends = ((MyApplication) this.getApplication()).getMyFriends();

        if(businessIndex.homefile.exists()){
            myFriends = businessIndex.ReadAllData("theContacts.json");
        }
        else{
            businessIndex.WriteAllData(myFriends, "theContacts.json");
            myFriends = businessIndex.ReadAllData("theContacts.json");
        }

        adapter = new ContactAdapter(MainActivity.this, myFriends);

        lv_friendsList.setAdapter(adapter);

        btn_edit.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Intent i = new Intent(v.getContext(), EditPage.class );
                startActivityForResult(i, 2);
            }
        });

        btn_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String searchedName = et_search.getText().toString();
                myFriends.setMyFriendsList(businessIndex.SearchFirstName(searchedName, myFriends));
                adapter.notifyDataSetChanged();
                //myFriends = businessIndex.ReadAllData("theContacts.json");
                //myFriends.setMyFriendsList(myFriends.getMyFriendsList());
            }
        });

        btn_sortAGE.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                refreshPage();
                Collections.sort(myFriends.getMyFriendsList(), new Comparator<Contact>() {
                    @Override
                    public int compare(Contact p1, Contact p2) {
                        return p1.getAge() - p2.getAge();
                    }
                });

                adapter.notifyDataSetChanged();
            }
        });

        btn_sortABC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                refreshPage();
                Collections.sort(myFriends.getMyFriendsList());
                adapter.notifyDataSetChanged();


            }
        });

        lv_friendsList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                openProfile(position);
            }
        });

    }

    private void openProfile(int position) {
        Intent i = new Intent(getApplicationContext(), ContactProfile.class);

        //get the contents of contact at position
        Contact p = myFriends.getMyFriendsList().get(position);

        i.putExtra("edit", position);
        i.putExtra("name", p.getFirstName());
        i.putExtra("lastName", p.getLastName());
        i.putExtra("age", p.getAge());
        i.putExtra("phone", p.getPhoneNum());
        i.putExtra("pictureNumber", p.getPictureNumber());
        i.putExtra("address", p.getLoc());

        startActivity(i);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {

        super.onActivityResult(requestCode, resultCode, data);


        if(requestCode == 2) {

            refreshPage();

        }

    }

    public void refreshPage(){
        myFriends = ((MyApplication) this.getApplication()).getMyFriends();

        myFriends = businessIndex.ReadAllData("theContacts.json");

        adapter = new ContactAdapter(MainActivity.this, myFriends);

        lv_friendsList.setAdapter(adapter);

        adapter.notifyDataSetChanged();
    }
}
